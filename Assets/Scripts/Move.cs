using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour{
    // Start is called before the first frame update
    public float forceValue;
    public float jumValue;
    private AudioSource audio;
    private Rigidbody rigidbod;
    private float posX;
    private float posY;
    private float posZ;

    
    void Start()
    {
        rigidbod = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
        posX = transform.position.x;
        posY = transform.position.y;
        posZ = transform.position.z;
    }

    // Update is called once per frame
    void Update(){
        //transform.Translate (Input.GetAxis ("Horizontal") * speed * Time.deltaTime, 0, (Input.GetAxis ("Vertical") * speed * Time.deltaTime));
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidbod.velocity.y) < 0.01f)
        {
            rigidbod.AddForce(Vector3.up * jumValue, ForceMode.Impulse);
            audio.Play();
        }
        if(Input.touchCount==1)
        if (Input.touches[0].phase == TouchPhase.Began && Mathf.Abs(rigidbod.velocity.y) < 0.01f)
        {
            rigidbod.AddForce(Vector3.up * jumValue, ForceMode.Impulse);
            audio.Play();
        }

        if (transform.position.y <= -10f)
        {
            gameObject.transform.position = new Vector3(posX, posY, posZ);
        }
        
    }

    void FixedUpdate()
    {
        rigidbod.AddForce (new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * forceValue);
        
        rigidbod.AddForce (new Vector3(Input.acceleration.x, 0, Input.acceleration.y) * forceValue);
    }

    void OnCollisionEnter(Collision colision)
    {
       
        switch (colision.gameObject.tag)
        {
            case "point":
                Destroy(colision.gameObject);
                break;
            case "completed":
                SceneManager.LoadScene("Level3");
                break;
        }
       
        
    }
}
